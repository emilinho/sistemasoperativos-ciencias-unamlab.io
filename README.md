# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/>

Semestre 2021-1

### Presentación del curso

+ [Presentación del curso](presentacion.md "Generalidades del curso")

### Flujo de trabajo

+ [Flujo de trabajo para la entrega de tareas y prácticas](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so#readme "fork y merge request")

### Prácticas

+ [Repositorio para la entrega de tareas y prácticas](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so "Las tareas se entregan a traves de merge-request")
+ [Índice de prácticas](https://SistemasOperativos-Ciencias-UNAM.gitlab.io/practicas/ "Índice de prácticas")

### Temas

+ [Guía rápida de `git`](temas/git.md "9418/tcp")
+ [Arquitectura y componentes de computadoras](temas/arch.md "arch.md")
+ [Historia de los sistemas operativos](temas/history.md "history.md")
+ [Inicio de una computadora x86 o x86_64 con Linux y Windows](temas/boot.md "boot.md")
+ [Tipos de _kernel_](temas/kernel.md "kernel.md")
+ [Directorios en Linux (LSB - FHS)](temas/lsb-fhs.md "lsb-fhs.md")
+ [Tipos de archivo en UNIX](temas/filetypes.md "filetypes.md")
+ [Programas y procesos](temas/ps-proc.md "ps-proc.md")

### Tareas

+ [Repositorio para la entrega de tareas y prácticas](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2021-1/tareas-so "Las tareas se entregan a traves de merge-request")
+ [Compilación e instalación de _software_ desde código fuente](tareas/compile.md)
+ [Programas de sistema de archivos, procesos, hilos, señales y sockets](tareas/programas.md)
+ [Módulos de _kernel_](tareas/linux-module.md)

### Ligas de interés

+ <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/>
+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/>
+ <https://tinyurl.com/ListaSO-2021-1>
+ <https://tinyurl.com/PizarronSO-2021-1>
+ <http://tinyurl.com/SO-Ciencias-UNAM-YouTube>
+ <https://tinyurl.com/SO-Ciencias-UNAM-Playlist>
+ <https://tinyurl.com/SO-Ciencias-UNAM-Videos>
+ <https://t.me/sistemasoperativos_ciencias_unam>
+ <https://groups.google.com/a/ciencias.unam.mx/group/sistemasoperativos-alumnos/>
+ <http://www.fciencias.unam.mx/asignaturas/713.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/presentacion/305778>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/305778>
+ <http://www.fciencias.unam.mx/docencia/horarios/20211/218/713>
+ <http://www.fciencias.unam.mx/docencia/horarios/20211/1556/713>
