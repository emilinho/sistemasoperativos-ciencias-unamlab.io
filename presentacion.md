# Universidad Nacional Autónoma de México
# Facultad de Ciencias
# Ciencias de la Computación

## Sistemas Operativos

Semestre 2021-1

+ José Luis Torres Rodríguez
+ Andrés Leonardo Hernández Bermúdez
+ Oscar Emilio Cabrera López

### Requisitos

+ Conocimientos básicos de GNU/Linux
+ Conocimientos de _arquitectura de computadoras_
+ Conocimientos de programación en un lenguaje _estructurado_ u _orientado a objetos_
+ No tener materias encimadas en el horario de _Sistemas Operativos_
+ Asistencia regular a clase
+ La asistencia a las sesiones de laboratorio es *obligatoria*

### Objetivos

+ Introducir al estudiante en el estudio de los elementos que conforman un sistema operativo
+ Presentar al estudiante los elementos necesarios para comprender la forma en que un sistema operativo controla los recursos de un sistemas de cómputo
+ Proporcionar al estudiante bases que le permitan explotar los recursos de un sistema de cómputo
+ Estudiar la estructura de los sistemas operativos tradicionales y sentar las bases para entender los sistemas operativos distribuidos
+ Llevar a cabo la implementación de algunas de las técnicas usadas en los sistemas operativos modernos, abordadas durante el curso, con el objetivo de reforzar la comprensión de las mismas

### Sistemas Operativos

+ GNU/Linux
+ Windows

### Lenguaje de programación

+ C

### Impartición de las clases

#### Teoría

Profesor

- Videos pre-grabados en su [canal de YouTube][canal-youtube-profesor]

Ayudante

- Miércoles de 19:00 a 20:00 horas [en esta liga de Google Meet][google-meet-ayudante]
- Transmision en vivo por Google Meet y las grabaciones se suben al [canal de YouTube][canal-youtube-ayudante]

#### Laboratorio

Dos sesiones en el [canal de Jitsi del laboratorio][canal-jitsi-laboratorio]

+ Martes de 18:30 a 20:00 h
  - Números de lista del 1 al 30
 
+ Jueves de 18:30 a 20:00 h
  - Números de lista del 31 al 60

+ [Canal de YouTube][canal-youtube-laboratorio]

### Contenido del curso

1. Introducción a los sistemas operativos

    1. Conceptos de sistemas operativos
    2. Administración y manejo de recursos
    3. Maquinas virtuales
    4. Elementos y estructura de un sistema operativo
    5. Tipos de sistemas operativos

2. Procesos

    1. Descripción de un proceso
    2. Creación y finalización de procesos
    3. Estado de un proceso
    4. Comunicación entre procesos
    5. Procesos y máquinas virtuales

3. Hilos

    1. Revisión del concepto de hilo
    2. Uso e implementación de hilos
    3. Modelos de hilo simple y multihilos
    4. Hilos en sistemas distribuidos

4. Planificación

    1. Conceptos básicos
    2. Criterios y algoritmos de planificación
    3. Planificación de hilos
    4. Planificación en sistemas distribuidos
    5. Problemas clásicos

5. Comunicación y sincronización de procesos

    1. Condiciones de carrera
    2. Región crítica
    3. Exclusión mutua y solución de Peterson
    4. Semáforos
    5. Monitores
    6. Problemas y ejemplos

6. Interbloqueo

    1. Modelado de interbloqueos
    2. Métodos para el manejo de interbloqueos
    3. Prevención y evitación de interbloqueos

7. Manejo de memoria principal

    1. Conceptos generales
    2. Intercambio
    3. Asignación de memoria contigua
    4. Paginación
    5. Tablas de páginas
    6. Segmentación
    7. Virtualización de la memoria

8. Manejo de memoria virtual

    1. Introducción
    2. Paginación sobre demanda
    3. Copy-on-write
    4. Reemplazo de páginas
    5. Manejo de marcos

9. Sistemas de archivos

    1. Conceptos generales
    2. Métodos de acceso
    3. Estructura de directorios y discos
    4. Estructura de un sistema de archivos
    5. Implementación de sistemas de archivos y directorios
    6. Métodos de asignación
    7. Manejo de espacio libre
    8. Recuperación de sistemas de archivos
    9. Archivos compartidos

10. Entrada y salida

    1. Introducción
    2. Hardware y software de entrada y salida
    3. Subsistema de entrada y salida en el Kernel
    4. Peticiones de entrada y salida y operaciones de hardware
    5. Virtualización de la entrada y salida

### Sesiones de laboratorio

+ Se llevarán a cabo una serie de prácticas cuya finalidad es que el alumno se familiarice con el lenguaje C.

+ Habrá dos proyectos principales: uno a mitad del curso y otro al finalizar. Estos proyectos tienen como objetivo fijar los conocimientos adquiridos en la parte teórica del curso.

+ Toda la información acerca de las sesiones del laboratorio se encuentra en la [sección de prácticas] de este repositorio

+ La asistencia a las sesiones de laboratorio es obligatoria

### Evaluación del semestre

Se considerarán los siguientes elementos para llevar a cabo la evaluación:

#### 1. Asistencia regular a clase

#### 2. Exámenes

Calendarización de examenes

| Examen	| Fecha				|
|--------------:|:------------------------------|
| Parcial 1	|                       	|
| Parcial 2	|                       	|
| Parcial 3	|                       	|

##### Requisitos para presentar examen

+ Haber entregado puntualmente las tareas anteriores a la fecha del examen
+ No habrá reposiciones de examenes
+ Es requisito indispensable el contar con un promedio aprobatorio en los examenes y haberlos presentado todos, sin excepción, para tener derecho a obtener una calificación final aprobatoria en el semestre

#### 3. Prácticas

+ Prácticas sobre cada uno de los temas principales vistos a lo largo del semestre
+ Es requisito indispensable el haber entregado todas las prácticas para tener derecho a obtener una calificación final aprobatoria en el semestre
+ No es posible obtener una calificación aprobatoria si no se ha cumplido con la entrega de las prácticas asignadas durante el semestre

#### 4. Exposiciones

+ Se propondrá una lista de temas a exponer "en equipo"
+ Cada equipo contará con al menos tres semanas para la preparación del tema correspondiente

#### 5. Tareas teórico-prácticas

+ En estas tareas se aplicarán todos los conocimientos vistos a la fecha, complementados con material adicional

#### 6. Participaciones en clase

+ Éstas son **OBLIGATORIAS** y son un elemento fundamental en la evaluación final
+ Se tomarán en cuenta sobre todo aquellas participaciones que refuercen lo visto en clase y que constituyan una aportación importante a la misma

### Notas

+ La entrega de todos los trabajos será improrrogable
+ Todos los trabajos duplicados serán evaluados con cero, sin hacer indagaciones
+ La calificación final se entregará personalmente a más tardar el día de la segunda vuelta de los examenes finales
+ Quien no se presente el día señalado a recibir su calificación está aceptando la evaluación que se haga de su trabajo durante el semestre
+ Sin excepción, no se aceptarán reclamaciones fuera de la fecha señalada en el punto anterior
+ Sin excepciones, no se permitirá renunciar a ninguna calificación
+ La calificación de NP sólo se asignará a quien no haya entregado ningún trabajo y no haya presentado ningún examen
+ De ninguna manera se dará un trato ni una calificación especial a ningún estudiante, por el hecho de trabajar, tener un promedio alto en su historial académico, contar con algún tipo de beca o tener materias encimadas
+ Tampoco se tomarán en cuenta recomendaciones de ninguna persona para asignar la calificación final

### Evaluación

La calificación final se calculará en base a los siguientes porcentajes:

| Elemento	|Valor	|
|--------------:|:------|
| Examenes	| 30%	|
| Prácticas	| 50%	|
| Tareas	| 20%	|

Las participaciones que aporten ideas a la clase pueden ayudar a incrementar la calificación final

### Observaciones adicionales

+ Se prohíben los teléfonos celulares durante la clase

    * Éstos deberán apagarse o ponerse en modo vibrador y no contestarse en el salón o laboratorio
    * Quien responda llamadas dentro del salón o laboratorio deberá retirarse, por respeto a sus compañeros

+ Por ningún motivo se repetirán clases anteriores a ninguna persona, por el hecho de no haber podido asistir a las mismas, a menos que la mayoría lo solicite y justifique con argumentos diferentes al mencionado

+ Al inicio del semestre se formarán equipos de trabajo, para llevar a cabo las tareas que lo requieran, tomando en cuenta lo siguiente:

    * El número de integrantes de los equipos se establecerá en base a la cantidad de inscritos
    * Una vez formados lo equipos no se permitirá hacer cambios de los integrantes
    * El trabajo en equipo no implica la división de la tarea por partes iguales entre los integrantes del mismo
    * Se dará por sentado que estos trabajos se desarrollaron colectivamente por todos los integrantes, por lo que no se evaluará cada parte individualmente; cada uno de los miembros de un equipo deberá poder defender, en su totalidad, el trabajo entregado

+ Las tareas y programas podrán ser enviados por correo o entregadas a través de Github, dependiendo de la forma en la que se solicite la entrega

    * <code><SistemasOperativos@ciencias.unam.mx></code>
    * <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/>

+  Las tareas y prácticas serán enviadas a través de una lista de correo en la que se dará de alta a todos los integrantes del grupo

+ Para las tareas que se entreguen en papel, se sugiere hacer uso de hojas recicladas

+ No es necesario hacer la entrega de los trabajos en fólder, sobre, etc

+ De cada trabajo entregado se les podrá solicitar – en cualquier momento – responder preguntas acerca de o adicionales al mismo. Todos los trabajos deberán ser entregados, o enviados por correo, personalmente

+ Se prohíbe introducir alimentos y bebidas al taller asignado para las sesiones prácticas

+ Ningún alumno deberá entrar al taller asignado para las sesiones prácticas, hasta que esté presente el titular, el ayudante o el laboratorista; tampoco deberá permanecer dentro después de finalizada la clase, a menos que se cuente con la autorización de la Coordinación de la Licenciatura en Ciencias de la Computación, para hacer uso de este espacio fuera del horario normal

### Bibliografía básica

>>>
+ Operating System Concepts, 9th edition, 2013
    Silverschatz, Abraham; Galvin, Peter; Gagne, Greg
    John Wiley & Sons

+ Sistemas Operativos Modernos, 3a edición, 2009
    Tanenbaum, Andrew S.
    Pearson/Prentice Hall

+ Fundamentos de Sistemas Operativos. Primera edición
    Golf, Gunnar; Ruiz, Esteban; Bergero, Federico; Meza, Erwin
    Universidad Nacional Autónoma de México, Instituto de Investigaciones Económicas, Facultad de Ingeniería. 2015
    <http://sistop.org/>
    Distribuido libremente bajo los términos de la CC BY-SA versión 4.0
>>>

### Ligas de interés

+ <https://SistemasOperativos-Ciencias-UNAM.gitlab.io/>
+ <https://gitlab.com/SistemasOperativos-Ciencias-UNAM/>
+ <https://tinyurl.com/ListaSO-2021-1>
+ <https://tinyurl.com/PizarronSO-2021-1>
+ <https://t.me/sistemasoperativos_ciencias_unam>
+ <https://groups.google.com/a/ciencias.unam.mx/group/sistemasoperativos-alumnos/>
+ <http://www.fciencias.unam.mx/asignaturas/713.pdf>
+ <http://www.fciencias.unam.mx/docencia/horarios/presentacion/305778>
+ <http://www.fciencias.unam.mx/docencia/horarios/detalles/305778>
+ <http://www.fciencias.unam.mx/docencia/horarios/20211/218/713>
+ <http://www.fciencias.unam.mx/docencia/horarios/20211/1556/713>

[salon-Y303]: http://www.fciencias.unam.mx/plantel/horariosalon/20191/449 "Salón 303 de Yelizcalli"
[Taller-Tlahuizcalpan]: http://www.fciencias.unam.mx/plantel/horariosalon/20211/258 "Taller de Sistemas Operativos, Redes de Cómputo, Sistemas Distribuidos y Manejo de Información"
[sección de prácticas]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/

[canal-youtube-profesor]: https://www.youtube.com/playlist?list=PLwziW0L5X87xtcbiI9y6vcpxdPsOOx3jh
[canal-youtube-ayudante]: https://www.youtube.com/playlist?list=PLa3Cxza-egQUMFXSpJKmfcAbq-KyYXn04
[canal-youtube-laboratorio]: https://www.youtube.com/playlist?list=PLvJCdq6rEbhq_JgISCEJp3gKp0V9ZrhmP
[canal-jitsi-laboratorio]: https://meet.jit.si/sistop-fciencias
[google-meet-ayudante]: https://meet.google.com/qgf-hcjn-ihs
